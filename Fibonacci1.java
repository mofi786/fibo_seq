import java.util.Scanner;

public class Fibonacci1{

     public static void main(String []args){
        //User enters Fib number
        System.out.print("Enter fibonacci number 'n': ");
        int num = new Scanner(System.in).nextInt();
        
        //Printing reg fib sequence
        /*for(int i=1; i<=num; i++){  System.out.print(fibonacci1(i) +" ");}*/
        //System.out.println();
        
        //Printing Swift Nav Fib sequence
        System.out.println("\nFibonacci seq for" + num +" numbers: ");
        for(int i=1; i<=num; i++){
            int result = fibonacci1(i);
            if (result%15==0){System.out.print("BuzzFizz ");}
            else{
                if (result%3==0){System.out.print("Buzz ");}
                else{
                    if (result%5==0){System.out.print("Fizz ");}
                    else{
                        if (isPrime(result)) {System.out.print("BuzzFizz ");}
                        else{System.out.print(Integer.toString(result)+" ");}
                    }}}}
    }
    
    public static int fibonacci1(int n)
    {
         if (n==1 || n==2){return 1;}
         return fibonacci1(n-1) + fibonacci1(n-2); 
    }
     
    public static boolean isPrime(int n) 
    {
        if (n==2){return true;}
        //check if n is a multiple of 2
        if (n%2==0 || n==1){return false;}
        //check if n is divisible by just the odds
        for(int i=3;i*i<=n;i+=2){
            if(n%i==0){return false;}
        }
        return true;
    }
}
